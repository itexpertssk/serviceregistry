# serviceregistry
Services registration server based on Netflix Eureka. Services requires Netflix Eureka client.

* Supports HA Service registry setup
* Spring also supports HashiCorp [Consul registry](https://spring.io/projects/spring-cloud-consul) implementation.
* In Kubernetes environment, you can discover services from Kubernetes registry using [Spring Cloud Kubernetes](https://spring.io/projects/spring-cloud-kubernetes)

# Try it
* The Eureka Web GUI http://localhost:8761/
 
 
